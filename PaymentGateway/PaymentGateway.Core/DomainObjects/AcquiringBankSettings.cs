﻿using PaymentGateway.Core.Interfaces;

namespace PaymentGateway.Core.DomainObjects
{
   

    public class AcquiringBankSettings : IAcquiringBankSettings
    {
        public string PaymentApiUrl { get; set; }
        public string EncryptionKey { get; set; }
    }
}
