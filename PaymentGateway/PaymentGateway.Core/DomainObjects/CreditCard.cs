﻿using PaymentGateway.Core.DomainObjects.Enums;


namespace PaymentGateway.Core.DomainObjects
{
    public class CreditCard
    {

        public string CardholderName { get; set; }
        public string CardNumber { get; set; }

        public Month Month { get; set; }

        public byte Year { get; set; }

        public string Cvv { get; set; }

    }
}
