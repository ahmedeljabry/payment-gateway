﻿namespace PaymentGateway.Core.DomainObjects
{
    public class PaymentRequest
    {
        public CreditCard Card { get; set; }

        public decimal Amount { get; set; }

        public string Currency { get; set; }
    }
}
