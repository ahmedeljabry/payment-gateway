﻿using System.Net;

namespace PaymentGateway.Core.DomainObjects
{
    public class PaymentResponse
    {
        public string TransactionNumber { get; set; }
        public HttpStatusCode StatusCode { get; set; }

        public string ErrorMessage { get; set; }

    }
}
