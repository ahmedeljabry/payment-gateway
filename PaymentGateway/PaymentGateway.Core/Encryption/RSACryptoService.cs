﻿using Newtonsoft.Json;
using PaymentGateway.Core.Interfaces;
using System;
using System.Security.Cryptography;
using System.Text;
using PaymentGateway.Core.DomainObjects;
using Microsoft.Extensions.Options;

namespace PaymentGateway.Core.Encryption
{
    public class RSACryptoService : ICryptoService
    {
        private readonly RSA _rsa;
        public RSACryptoService(IOptions<AcquiringBankSettings> acquiringBankConfiguration)
        {
            _rsa = RSA.Create();
            _rsa.FromXmlString(acquiringBankConfiguration.Value.EncryptionKey);
        }
        public string Decrypt(string cipherText)
        {
            var encoding = _rsa.Decrypt(
                Convert.FromBase64String(cipherText),
                RSAEncryptionPadding.Pkcs1);
            return Encoding.UTF8.GetString(encoding);
        }

        public string Encrypt(PaymentRequest obj)
        {
           var content=  JsonConvert.SerializeObject(obj);
           return Encrypt(content);

        }

        public string Encrypt(string plainText)
        {
            var encoding = Encoding.UTF8.GetBytes(plainText);
            var cipherText = _rsa.Encrypt(encoding, RSAEncryptionPadding.Pkcs1);
            return Convert.ToBase64String(cipherText);
        }
    }
}
