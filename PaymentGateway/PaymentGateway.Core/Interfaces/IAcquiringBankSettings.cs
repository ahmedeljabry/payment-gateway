﻿namespace PaymentGateway.Core.Interfaces
{

    public interface IAcquiringBankSettings
    {
        string PaymentApiUrl { get; set; }
        string EncryptionKey { get; set; }
    }
}
