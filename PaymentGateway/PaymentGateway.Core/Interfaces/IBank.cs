﻿using PaymentGateway.Core.DomainObjects;
using System.Threading.Tasks;

namespace PaymentGateway.Core.Interfaces
{
    public interface IBank
    {
        Task<PaymentResponse> SendPaymentRequestAsync(string request);
    }
}
