﻿using PaymentGateway.Core.DomainObjects;

namespace PaymentGateway.Core.Interfaces
{
    public interface ICryptoService
    {
        string Encrypt(PaymentRequest obj);

        string Encrypt(string  plainText);

        string Decrypt(string cipherText);
    }
}
