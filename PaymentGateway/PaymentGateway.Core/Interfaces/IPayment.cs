﻿using PaymentGateway.Core.DomainObjects;
using System.Threading.Tasks;
using PaymentGateway.Data.Entities;

namespace PaymentGateway.Core.Interfaces
{
    public interface IPayment
    { 
        Task<PaymentResponse> HandlePaymentAsync(PaymentRequest request);

        Task<Payment> GetAsync(string transactionId);
    }
}
