﻿using Microsoft.Extensions.Options;
using PaymentGateway.Core.DomainObjects;
using PaymentGateway.Core.Interfaces;
using PaymentGateway.Data.Entities;
using PaymentGateway.Data.Interfaces;
using System;
using System.Threading.Tasks;

namespace PaymentGateway.Core.Services.Domain
{
    public class PaymentService : IPayment
    {
        private readonly IBank _bankService;
        private readonly IPaymentRepository _paymentRepository;
        private readonly ICryptoService _cryptoService;
        private readonly IAcquiringBankSettings _acquiringBankConfiguration;
        public PaymentService(IBank bankService, 
            IPaymentRepository paymentRepository,ICryptoService encryptor, 
            IOptions<AcquiringBankSettings> acquiringBankConfiguration)
        {
            _bankService = bankService;
            _paymentRepository = paymentRepository;
            _cryptoService = encryptor;
            _acquiringBankConfiguration = acquiringBankConfiguration.Value;
        }

        public async Task<Payment> GetAsync(string transactionId)
        {


            var paymentHistory=await _paymentRepository.GetAsync(transactionId);

            if (paymentHistory == null) return null;

            paymentHistory.CardNumber = _cryptoService.Decrypt(paymentHistory.CardNumber);

            return paymentHistory;
        }

        public async Task<PaymentResponse> HandlePaymentAsync(PaymentRequest request)
        {

            var encryptedObj = _cryptoService.Encrypt(request);

            var response = await _bankService.SendPaymentRequestAsync(encryptedObj);

            await SaveTransaction(request, response);

            return response;
        }

        private async Task SaveTransaction(PaymentRequest request, PaymentResponse response)
        {
            var encryptedCardNumber =
                _cryptoService.Encrypt(request.Card.CardNumber.Substring(request.Card.CardNumber.Length - 4));
             
            var history = new Payment()
            {
                TransactionNumber = response.TransactionNumber,
                Status =(ushort) response.StatusCode,
                Year = request.Card.Year,
                Amount = request.Amount,
                Currency = request.Currency,
                ErrorMessage = response.ErrorMessage,
                Month = request.Card.Month.ToString(),
                CardNumber = encryptedCardNumber,
                Date = DateTime.UtcNow
            };
            await _paymentRepository.AddAsync(history);

           
        }
    }
}
