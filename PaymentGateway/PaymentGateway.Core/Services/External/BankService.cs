﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using PaymentGateway.Core.DomainObjects;
using PaymentGateway.Core.Interfaces;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace PaymentGateway.Core.Services.External
{
    public class BankService : IBank
    {
       
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ILogger<BankService> _logger;
        private readonly IAcquiringBankSettings _acquiringBankConfiguration;
        public BankService(IHttpClientFactory httpClientFactory,
            ILogger<BankService> logger,
            IOptions<AcquiringBankSettings> acquiringBankConfiguration)
        {
            _httpClientFactory = httpClientFactory;
            _logger = logger;
            _acquiringBankConfiguration = acquiringBankConfiguration.Value;
        }
        public async Task<PaymentResponse> SendPaymentRequestAsync(string request)
        {

            var httpClient = _httpClientFactory.CreateClient();

            var httpContent = new StringContent(request);

            PaymentResponse result;

            try
            {
                var response = await httpClient.PostAsync(_acquiringBankConfiguration.PaymentApiUrl, httpContent);

                var content = await response.Content.ReadAsStringAsync();

                result = JsonConvert.DeserializeObject<PaymentResponse>(content);
                result.StatusCode = response.StatusCode;
            }
            catch (Exception e)
            {
                result = new PaymentResponse()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    ErrorMessage = "Something went wrong"
                };
                _logger.LogError(exception: e, message: result.ErrorMessage);
            }

            return result;


        }
    }
}
