﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PaymentGateway.Data.Entities;

namespace PaymentGateway.Data.Configurations
{


    internal class PaymentConfiguration : IEntityTypeConfiguration<Payment>
    {
        public void Configure(EntityTypeBuilder<Payment> builder)
        {
            builder.ToTable("Payment");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.TransactionNumber).
                HasColumnType("varchar(200)").
                IsRequired();


            builder.Property(c =>c.CardNumber ).
                HasColumnType("nvarchar(1000)").
                IsRequired();

            builder.Property(c => c.Month).
                HasColumnType("varchar(20)").
                IsRequired();

            builder.Property(c => c.Year).
                HasColumnType("tinyint").
                IsRequired();
            builder.Property(c => c.Amount).
                HasColumnType("numeric(18, 0)").
                IsRequired();
            builder.Property(c => c.Currency).
                HasColumnType("varchar(10)").
                IsRequired();

            builder.Property(c => c.Status).
                HasColumnType("smallint").
                IsRequired();

            builder.Property(c => c.ErrorMessage).
                HasColumnType("nvarchar(200)").
                IsRequired(false);

            builder.Property(c => c.Date).ValueGeneratedOnAdd();

        }
    }
}
