﻿using Microsoft.EntityFrameworkCore;
using PaymentGateway.Data.Configurations;
using PaymentGateway.Data.Entities;
using PaymentGateway.Data.Interfaces;

namespace PaymentGateway.Data.Context
{


    public class PaymentDbContext:DbContext, IPaymentDbContext
    {
        public DbSet<Payment> Payment { get; set; }

        public PaymentDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new PaymentConfiguration());
            modelBuilder.Entity<Log>(entity =>
            {
                entity.ToTable("Logs", "dbo");


            });

        }
    }
}
