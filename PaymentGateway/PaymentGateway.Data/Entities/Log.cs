﻿using System;

namespace PaymentGateway.Data.Entities
{
    public class Log
    {
        public int Id { get; set; }
        public string Application { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
        public string StackTrace { get; set; }
        public string Level { get; set; }
        public string Logger { get; set; }
        public string CallSite { get; set; }
        public string Type { get; set; }
        public string AdditionalInfo { get; set; }
        public DateTime Date { get; set; }

    }
}
