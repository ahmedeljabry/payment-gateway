﻿using System;

namespace PaymentGateway.Data.Entities
{
    public class Payment
    {
        public int Id { get; set; }
        public string TransactionNumber { get; set; }

        public string CardNumber { get; set; }
        public string Month { get; set; }
        public byte Year { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }

        public ushort Status { get; set; }

        public string ErrorMessage { get; set; }

        public DateTime Date { get; set; }

    }
}
