﻿using Microsoft.EntityFrameworkCore;
using PaymentGateway.Data.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace PaymentGateway.Data.Interfaces
{
    public interface IPaymentDbContext
    {
        DbSet<Payment> Payment { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}
