﻿using PaymentGateway.Data.Entities;
using System.Threading.Tasks;

namespace PaymentGateway.Data.Interfaces
{
    public interface IPaymentRepository
    {
        Task AddAsync(Payment paymentHistory);

        Task<Payment> GetAsync(string transactionNumber);
    }
}
