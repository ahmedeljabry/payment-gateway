﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PaymentGateway.Data.Entities;
using PaymentGateway.Data.Interfaces;
using System;
using System.Threading.Tasks;

namespace PaymentGateway.Data.Repository
{
    public class PaymentRepository : IPaymentRepository
    {
        private readonly IPaymentDbContext _context;
        private readonly ILogger<PaymentRepository> _logger;
        public PaymentRepository(IPaymentDbContext context, ILogger<PaymentRepository> logger)
        {
            _context = context;
            _logger = logger;
        }
        public async Task AddAsync(Payment paymentHistory)
        {
            await _context.Payment.AddAsync(paymentHistory);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                var serializedObject = JsonConvert.SerializeObject(paymentHistory);

                _logger.LogError(exception: e, message: $"Failed to save PaymentHistory : {serializedObject}");
            }

        }

        public async Task<Payment> GetAsync(string transactionNumber)
        {
            try
            {
                return await _context.Payment.FirstOrDefaultAsync(c => c.TransactionNumber == transactionNumber);
            }
            catch (Exception e)
            {
                _logger.LogError(exception: e, message: $"Failed to retrieve data from paymentHistory with  : {transactionNumber}");
                return null;
            }

        }
    }
}
