﻿using FluentAssertions;
using Newtonsoft.Json;
using PaymentGateway.WebApp;
using PaymentGateway.WebApp.V1.Models;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PaymentGateway.IntegrationTests.API
{
    [Trait("Category", TestCategory.IntegrationTest)]
    public class PaymentApiTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public PaymentApiTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        [Trait("Category", TestCategory.PositiveCases)]
        public async Task SendPayment_ValidPaymentDto_200StatusCodeWithTransactionNumber()
        {
            //Act
            var request = TestHelper.CreatePaymentDto();
            var requestJson = JsonConvert.SerializeObject(request);

            var httpContent = new StringContent(requestJson, Encoding.UTF8, "application/json");
            

            //Arrange
            var response = await _client.PostAsync("/api/Payment", httpContent);

            var content = await response.Content.ReadAsStringAsync();

            var dto = JsonConvert.DeserializeObject<PaymentResponseDto>(content);

            //Assert
            response.EnsureSuccessStatusCode();

            dto.StatusCode.Should().Be(HttpStatusCode.OK);

            dto.TransactionNumber.Should().NotBeNull();

        }

        [Fact]
        [Trait("Category", TestCategory.NegativeCases)]
        public async Task SendPayment_InvalidValidPaymentDto_400WithErrorMessage()
        {
            //Act
            var request = TestHelper.CreatePaymentDto();

            request.Card.CardNumber = string.Empty;

            var requestJson = JsonConvert.SerializeObject(request);

            var httpContent = new StringContent(requestJson, Encoding.UTF8, "application/json");
            
            //Arrange
            var response = await _client.PostAsync("/api/Payment", httpContent);

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);


        }


        [Fact]
        [Trait("Category", TestCategory.PositiveCases)]
        public async Task GetPaymentDetails_ValidTransactionNumber_200StatusCodeWithPaymentObject()
        {
            //Act
            var request = TestHelper.CreatePaymentDto();
            var requestJson = JsonConvert.SerializeObject(request);

            var httpContent = new StringContent(requestJson, Encoding.UTF8, "application/json");


            //Arrange
            var submitResponse = await _client.PostAsync("/api/Payment", httpContent);

            var submitContent = await submitResponse.Content.ReadAsStringAsync();

            var paymentResponseDto = JsonConvert.DeserializeObject<PaymentResponseDto>(submitContent);

            var response = await _client.GetAsync($"/api/payment/{paymentResponseDto.TransactionNumber}");

            var content = await response.Content.ReadAsStringAsync();

            var paymentDto = JsonConvert.DeserializeObject<PaymentDto>(content);

            //Assert
            response.EnsureSuccessStatusCode();

            paymentDto.TransactionNumber.Should().BeEquivalentTo(paymentResponseDto.TransactionNumber);

            paymentDto.CardNumber.Substring(paymentDto.CardNumber.Length - 4).Should()
                .BeEquivalentTo(request.Card.CardNumber.Substring(request.Card.CardNumber.Length - 4));

        }

        [Fact]
        [Trait("Category", TestCategory.NegativeCases)]
        public async Task SendPayment_InvalidValidTransactionNumber_400WithErrorMessage()
        {
            //Act
            const string transactionNumber="000000";

            //Arrange
            var response = await _client.GetAsync($"/api/payment/{transactionNumber}");

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);


        }

    }
}
