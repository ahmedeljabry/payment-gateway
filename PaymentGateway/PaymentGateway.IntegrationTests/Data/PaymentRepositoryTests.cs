﻿using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using PaymentGateway.Data.Interfaces;
using PaymentGateway.Data.Repository;
using System;
using System.Threading.Tasks;
using Xunit;

namespace PaymentGateway.IntegrationTests.Data
{
    [Trait("Category", TestCategory.IntegrationTest)]
    public class PaymentRepositoryTests : RepositoryTestBase
    {
        private readonly IPaymentRepository _repository;

        public PaymentRepositoryTests()
        {
            var mockLogger = new Mock<ILogger<PaymentRepository>>();
            _repository = new PaymentRepository(_dbContext, mockLogger.Object);
        }

      
        [Fact]
        [Trait("Category", TestCategory.PositiveCases)]
        public async Task Add_Payment_SavePayment()
        {
            
            //Arrange
            var payment = TestHelper.CreatePayment();
            
            //Act
            await _repository.AddAsync(payment);

            var result = await _repository.GetAsync(payment.TransactionNumber);

            //Assert
            result.Should().NotBeNull();

            result.CardNumber.Should().BeEquivalentTo(payment.CardNumber);


        }

        [Fact]
        [Trait("Category", TestCategory.PositiveCases)]
        public async Task Get_TransactionNumber_PaymentObject()
        {

            //Arrange
            var list = TestHelper.CreatePaymentList();

            foreach (var payment in list)
            {
                await _repository.AddAsync(payment);

            }

            //Act
            var randomTarget = list[new Random().Next(list.Count)];
            var result = await _repository.GetAsync(randomTarget.TransactionNumber);

            //Assert
            result.Should().NotBeNull();

            result.CardNumber.Should().BeEquivalentTo(randomTarget.CardNumber);


        }

        [Fact]
        [Trait("Category", TestCategory.NegativeCases)]
        public async Task Get_InvalidTransactionNumber_Null()
        {

            //Arrange
            var payment = TestHelper.CreatePayment();

            await _repository.AddAsync(payment);

            //Act
            var result = await _repository.GetAsync("12345");

            //Assert
            result.Should().BeNull();

        }


    }
}
