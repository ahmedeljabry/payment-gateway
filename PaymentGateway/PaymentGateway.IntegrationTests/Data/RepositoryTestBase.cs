﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using PaymentGateway.Data.Context;
using PaymentGateway.Data.Interfaces;

namespace PaymentGateway.IntegrationTests.Data
{
    public abstract class RepositoryTestBase 
    {
        protected IPaymentDbContext _dbContext;
        protected RepositoryTestBase()
        {
            var serviceCollection = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            var builder=new DbContextOptionsBuilder<PaymentDbContext>();
            builder.UseInMemoryDatabase("InMemoryPaymentDB").UseInternalServiceProvider(serviceCollection);

            _dbContext=new PaymentDbContext(builder.Options);
        }
       
    }
}
