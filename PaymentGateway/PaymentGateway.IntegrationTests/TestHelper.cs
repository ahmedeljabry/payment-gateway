﻿using PaymentGateway.Core.DomainObjects.Enums;
using PaymentGateway.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using PaymentGateway.WebApp.V1.Models;

namespace PaymentGateway.IntegrationTests
{
    internal static class TestHelper
    {

        internal static Payment CreatePayment()
        {
            return new Payment
            {
               
                CardNumber = Guid.NewGuid().ToString(),
                ErrorMessage = string.Empty,
                TransactionNumber = Guid.NewGuid().ToString(),
                Date = DateTime.Now,
                Currency = "EUR",
                Year = 22,
                Month = Month.December.ToString(),
                Status = 200,
                Amount = 200

            };
        }

        internal static IList<Payment> CreatePaymentList()
        {
            return Enumerable.Range(0, 10).Select(_ => CreatePayment()).ToList();
        }

        internal static PaymentRequestDto CreatePaymentDto()
        {
            return new PaymentRequestDto
            {
                Card = new CreditCardDto
                {
                    CardNumber = "5378344181229464",
                    Month = Month.April,
                    Year = 22,
                    CardholderName = "Test Holder",
                    Cvv = "025"

                },
                Currency = "EUR",
                Amount = 200
            };
        }

    }
}
