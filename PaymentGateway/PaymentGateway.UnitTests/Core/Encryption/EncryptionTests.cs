﻿using FluentAssertions;
using Microsoft.Extensions.Options;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using PaymentGateway.Core.DomainObjects;
using PaymentGateway.Core.Encryption;
using PaymentGateway.Core.Interfaces;

namespace PaymentGateway.UnitTests.Core.Encryption
{
    [Category(TestCategory.UnitTest)]
    public class EncryptionTests
    {
        private const string RSA_KEYS =
            "<RSAKeyValue><Modulus>7ewmEMj4xNRr4uo292cFuCPBIjHfw04bhjscJXMKtrZqgBRkGFjOLsnO0PRxeTEprhORRb/KAFIaf/hQ0yZxll+FMBpu3FK61vOZyYbj9xsFeAa5MTU+2e5KCTCygCeZIbxmCuJCobuWRBdISLmZ6yCnOFFZci/lzKz9fhkVDtutc+NfzIhomJ+Fe7+hupImirtHpePy/8SKRxNdG6WaltlWx9AGgHVh3en2vcoBSomfjMK+28wX5ySRLNBjXFVK8K2yklC5SBkTKdjWBhM7NOxJh8pQpVhoPFjjpq+QwjRcWWSQkFkQal3Kb7vvHrcRsaHjnLJnl42a0KSrnFTMtQ==</Modulus><Exponent>AQAB</Exponent><P>+L0wFguVa04a32D63KxtWixJpjiChlUFmmYEs47GfuiPROkwTia3sJ7M1LJr2KGQvA5F1em3zh5LsZsYzDJTz+3yfitILT427Yg/diVNZ81e6FBrHJHUhCwsxQ3nRawBOmM0DYW+jIG1P9HXw3SudrpFkkiivsNZUEQCma4fozc=</P><Q>9N4hHH6CzSfBonkzrNC3Ww2XsayBYAEmjqOhd3c1Mae5WDv9LWT4qp2BNfLI4FjP7SmbSRSOZOLzDHK1i3El0SMJJZXcQ3/XsOb2XGb+U1mnvx2RUJwYFwL8ka6Imx0qV/f0IiYirxRxemTumMi1W0vHR2gorbLCVg3q/hmI3XM=</Q><DP>lUTrc39WFsQTalQHl6A+K2wCcqbc5YJmT2bDkYGv6v7i42bRH0Uq3OofX9EG2UTz13b1P873B9G0eQvrULcvBsvzxvVwZf489kTaWKtADXf4aY/BB68YCo17/HFAhdYYEpLv7OGDHUzFmVvzkI8YF5XnJEdEqxgOJBX8PX956vc=</DP><DQ>IovMJBb9ITCAswlibvkrRR9PzXNG9WJ0LB+NwisBSDYfggFEoMjkBD4gYpoUGQKuowkBrXfG9xjFrXXpgvi9mxnUSocwAh3hMkYDb9nf+6ZkciQbwkGjbGuxcmQrRacBuFN1D/RNZGH/+IKQ3uFCJNU2Ei7xvj/AByADjUHQ+10=</DQ><InverseQ>x3AYZfb2Y2GEblzVgSYjHlPFOBwp1DfzNkGCOfKeF5lwkKUHTxL6ELZQuxl1fLHTUgqnzz2a7kT635ccnkWoKlAPShjL5Vv7AvPR6/7RJ4hX3PQootoaFdqM+Uco297iApS5JBHErWLM8g08UNK3eD/Wx9wKBiMzoEVXOu1ROBs=</InverseQ><D>aNdJwtW8gYeVbu2EgSqdo5Hl1jqh/r8Zc+HyOjJigzcI4R2bKftgB9AX2Y4NvrjCRBoNRu4HhnKniMKUYycqoHfBCfxr45x1BqlbmClfpRD8LPLRgnSTGJdfeoNB9ST93pbVZHBlqj0uLx0PVjSrNagy56c5Qg8HNVT5cyp8VAnx9u4bEY68N2U14Wk9hDwI1fbjJlvUWJt0JgxLKK6yGGiH1LnJf6iejnJV7FNEBUa3nZm6x3Pr9L58dMqC/deWGKOu0c+wu1t2KpQQwQX/UKBi+Pn4BTS0q93LO6IK2jNcRzK2A5cmaslE1cSpD6+Oe/5128Tx3sAHXBsymuG9tQ==</D></RSAKeyValue>";
        private ICryptoService _cryptoService;
       
        private Mock<IOptions<AcquiringBankSettings>> _mockAcquiringBankSettings;

        [SetUp]
        public void Setup()
        {
            _mockAcquiringBankSettings = new Mock<IOptions<AcquiringBankSettings>>();
            _mockAcquiringBankSettings.
                Setup(c => c.Value).
                Returns(new AcquiringBankSettings() { EncryptionKey = RSA_KEYS });
            _cryptoService = new RSACryptoService(_mockAcquiringBankSettings.Object);
        }

        [TearDown]
        public void TearsDown()
        {
            _mockAcquiringBankSettings = null;
            _cryptoService = null;
        }

        [Test]
        [Category(TestCategory.PositiveCases)]
        public void Encrypt_PaymentRequest_EncryptedObject()
        {
            //Arrange
            var paymentRequest = TestHelper.CreatePaymentRequest();

            //Act
            var cipherText = _cryptoService.Encrypt(paymentRequest);

            //Assert
            cipherText.Should().BeOfType<string>();

            var jsonObject = _cryptoService.Decrypt(cipherText);

            var result = JsonConvert.DeserializeObject<PaymentRequest>(jsonObject);

            result.Should().BeEquivalentTo(paymentRequest);
        }


        [Test]
        [Category(TestCategory.PositiveCases)]
        public void Encrypt_PlainText_EncryptedText()
        {
            //Arrange
            const string plainText = "plaint text";

            //Act
            var cipherText = _cryptoService.Encrypt(plainText);

            //Assert
            cipherText.Should().BeOfType<string>();

            var result = _cryptoService.Decrypt(cipherText);


            result.Should().BeEquivalentTo(plainText);
        }

    }
}
