﻿using FluentAssertions;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;
using PaymentGateway.Core.DomainObjects;
using PaymentGateway.Core.Interfaces;
using PaymentGateway.Core.Services.Domain;
using PaymentGateway.Data.Interfaces;
using System.Threading.Tasks;
using PaymentGateway.Data.Entities;

namespace PaymentGateway.UnitTests.Core.Services
{
    [Category(TestCategory.UnitTest)]
    public class PaymentServiceTests
    {
        private IPayment _paymentService;
        private Mock<IBank> _mockBankService;
        private Mock<IPaymentRepository> _mockRepository;
        private Mock<ICryptoService> _mockCryptoService;
        private Mock<IOptions<AcquiringBankSettings>> _mockAcquiringBankSettings;
        
        [SetUp]
        public void Setup()
        {
           
            _mockAcquiringBankSettings = new Mock<IOptions<AcquiringBankSettings>>();
            _mockAcquiringBankSettings.
                Setup(c => c.Value).
                Returns(new AcquiringBankSettings());
            _mockBankService =new Mock<IBank>();
            _mockRepository=new Mock<IPaymentRepository>();
            _mockCryptoService=new Mock<ICryptoService>();
            _paymentService=new PaymentService(
                _mockBankService.Object,
                _mockRepository.Object,
                _mockCryptoService.Object,
                _mockAcquiringBankSettings.Object);
        }

        [TearDown]
        public void TearDown()
        {
            _paymentService = null;
            _mockAcquiringBankSettings = null;
            _mockBankService = null;
            _mockRepository = null;
            _mockAcquiringBankSettings = null;
        }

        [Test]
        [Category(TestCategory.PositiveCases)]
        public async Task HandlePayment_PaymentRequest_SuccessfulPaymentResponse()
        {

            //Arrange
            var response = TestHelper.CreatePaymentResponse();

            var request = TestHelper.CreatePaymentRequest();

           _mockBankService.Setup(c => c.SendPaymentRequestAsync(string.Empty))
                .Returns(Task.FromResult(response));

            _mockRepository.Setup(c => c.AddAsync(null))
                .Returns(Task.CompletedTask);

            _mockCryptoService.Setup(c => c.Encrypt(request)).Returns(string.Empty);

            _mockCryptoService.Setup(c => c.Encrypt(string.Empty)).Returns(string.Empty);

            //Act
            var result=await _paymentService.HandlePaymentAsync(request);

            //Assert 
            result.Should().BeEquivalentTo(response);
        }


        [Test]
        [Category(TestCategory.PositiveCases)]
        public async Task Get_ValidTransactionNumber_PaymentHistoryObject()
        {

            //Arrange
            const string transactionNumber = "12354";

            var paymentHistory = TestHelper.CreatePayment();


            _mockRepository.Setup(c => c.GetAsync(transactionNumber))
                .Returns(Task.FromResult(paymentHistory));

           
            _mockCryptoService.Setup(c => c.Decrypt(paymentHistory.CardNumber)).
                Returns(paymentHistory.CardNumber);

            //Act
            var result = await _paymentService.GetAsync(transactionNumber);

            //Assert 
            result.Should().BeEquivalentTo(paymentHistory);
        }



        [Test]
        [Category(TestCategory.NegativeCases)]
        public async Task Get_InvalidTransactionNumber_Null()
        {

            //Arrange
            const string transactionNumber = "12354";

            _mockRepository.Setup(c => c.GetAsync(transactionNumber))
                .Returns(Task.FromResult((Payment)null));


            //Act
            var result = await _paymentService.GetAsync(transactionNumber);

            //Assert 
            result.Should().Be(null);
        }
    }
}
