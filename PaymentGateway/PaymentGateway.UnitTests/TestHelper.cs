﻿using PaymentGateway.Core.DomainObjects;
using PaymentGateway.Core.DomainObjects.Enums;
using PaymentGateway.Data.Entities;
using System;
using System.Net;

namespace PaymentGateway.UnitTests
{
    internal static class TestHelper
    {
       
        internal static PaymentRequest CreatePaymentRequest()
        {
            return new PaymentRequest
            {
                Card = new CreditCard
                {
                    CardNumber = "123456987",
                    Month = Month.April,
                    Year = 12,
                    CardholderName = "Test",
                    Cvv = "013"
                },
                Currency = "USD",
                Amount = 200
            };
        }

        internal static PaymentResponse CreatePaymentResponse()
        {
            return  new PaymentResponse
            {
                StatusCode = HttpStatusCode.OK,
                TransactionNumber = Guid.NewGuid().ToString(),
                ErrorMessage = string.Empty
            };
        }

        internal static Payment CreatePayment()
        {
            return new Payment
            {
                Id= new Random().Next(int.MinValue, int.MaxValue),
                CardNumber = Guid.NewGuid().ToString(),
                ErrorMessage = string.Empty,
                TransactionNumber = Guid.NewGuid().ToString(),
                Date = DateTime.Now,
                Currency = "EUR",
                Year = 22,
                Month = Month.December.ToString(),
                Status = 200,
                Amount = 200

            };
        }

      
    }
}
