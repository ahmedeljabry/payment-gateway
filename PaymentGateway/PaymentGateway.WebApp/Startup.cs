using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using PaymentGateway.Core.Interfaces;
using PaymentGateway.Core.Services.Domain;
using PaymentGateway.Core.Services.External;
using System;
using System.IO;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using PaymentGateway.Core.DomainObjects;
using PaymentGateway.Core.Encryption;
using PaymentGateway.Data.Context;
using PaymentGateway.Data.Interfaces;
using PaymentGateway.Data.Repository;

namespace PaymentGateway.WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            var conStr = Configuration.GetConnectionString("DefaultConnection");

            services.AddDbContext<PaymentDbContext>(options => 
                options.UseSqlServer(conStr, x => x.MigrationsAssembly("PaymentGateway.WebApp")));
           
            services.AddControllers();

            services.Configure<AcquiringBankSettings>(Configuration.GetSection("AppSettings:AcquiringBankConfiguration"));

            services.AddApiVersioning(cfg =>
            {
                cfg.DefaultApiVersion = new ApiVersion(1, 0);
                cfg.AssumeDefaultVersionWhenUnspecified = true;
                cfg.ReportApiVersions = true;
                cfg.ApiVersionReader = ApiVersionReader.Combine(
                    new QueryStringApiVersionReader("v"),
                    new HeaderApiVersionReader("x-version"));
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                    { 
                        Title = "Payment Gateway API", 
                        Version = "V1",
                        Description = "Payment Gateway API"

                    });
                    
                c.IncludeXmlComments(Path.Combine(
                    AppContext.BaseDirectory, 
                    $"{Assembly.GetExecutingAssembly().GetName().Name}.xml"));
            });


            services.AddScoped<IPaymentDbContext, PaymentDbContext>();

            services.AddScoped<IPaymentRepository, PaymentRepository>();

            services.AddScoped<IPayment, PaymentService>();

            services.AddAutoMapper(typeof(Startup));

            services.AddHttpClient();

            services.AddSingleton<IBank,BankService>();

            services.AddSingleton<ICryptoService, RSACryptoService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
           
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            app.UseSwaggerUI(c=>{
                c.SwaggerEndpoint("/swagger/v1/swagger.json","Payment Gateway API V1");
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
