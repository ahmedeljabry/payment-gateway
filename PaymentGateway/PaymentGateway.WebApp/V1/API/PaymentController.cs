﻿using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PaymentGateway.Core.DomainObjects;
using PaymentGateway.Core.Interfaces;
using PaymentGateway.WebApp.V1.Models;

namespace PaymentGateway.WebApp.V1.API
{
    [Route("api/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    public class PaymentController : ControllerBase
    {
        private readonly IPayment _paymentService;
        private readonly IMapper _mapper;
        private readonly ILogger<PaymentController> _logger;
        public PaymentController(IPayment paymentService,IMapper mapper, ILogger<PaymentController> logger)
        {
            _paymentService = paymentService;
            _mapper = mapper;
            _logger = logger;

        }
        /// <summary>
        /// Submit payment request.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /PaymentRequestDto
        ///     {
        ///       "card": {
        ///         "CardholderName" : "Cardholder Name",
        ///         "cardNumber": "5378344181229464",
        ///         "month": 2,
        ///         "year": 20,
        ///         "cvv": "123"        
        ///       },
        ///       "amount": 500,
        ///       "currency": "EUR"
        ///     }
        ///
        /// </remarks>
        /// <param name="dto"></param>
        /// <returns>status of the payment request</returns>
        /// <response code="200">Returns transaction number</response>
        /// <response code="500">Unsuccessful payment</response>
        /// <response code="400">Bad Request</response>
        [HttpPost]
        public async Task<IActionResult> Post(PaymentRequestDto dto)
        {
           
            var request = _mapper.Map<PaymentRequest>(dto);

            _logger.LogInformation("Payment transaction started");

            var response =await _paymentService.HandlePaymentAsync(request);

            var responseDto = _mapper.Map<PaymentResponseDto>(response);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                _logger.LogInformation("Transaction succeeded", responseDto.TransactionNumber);
             
                return Ok(responseDto);
            }

            _logger.LogError("Transaction failed", responseDto.ErrorMessage);

            return BadRequest(responseDto);
           
        }



        /// <summary>
        /// Get Payment Details.
        /// </summary>
        /// <param name="transactionNumber"></param>
        /// <returns>Payment details</returns>
        /// <response code="200"></response>
        /// <response code="500">Internal server error </response>
        /// <response code="400">Bad Request</response>

        [HttpGet("{transactionNumber}")]
        public async Task<IActionResult> Get(string transactionNumber)
        {
            var paymentHistory =await _paymentService.GetAsync(transactionNumber);

            if (paymentHistory == null)
            {
                return BadRequest($"There is no payment history for {transactionNumber}");
            }

            var paymentDto = _mapper.Map<PaymentDto>(paymentHistory);

            return Ok(paymentDto);
        }

    }
}