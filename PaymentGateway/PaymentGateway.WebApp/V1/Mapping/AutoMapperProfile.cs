﻿using AutoMapper;
using PaymentGateway.Core.DomainObjects;
using PaymentGateway.Data.Entities;
using PaymentGateway.WebApp.V1.Models;

namespace PaymentGateway.WebApp.V1.Mapping
{
    public class AutoMapperProfile:Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<CreditCardDto, CreditCard>(); 
            CreateMap<PaymentRequestDto, PaymentRequest>();
            CreateMap<Payment, PaymentDto>()
                .ForMember(dest => dest.CardNumber,
                    opt => opt.MapFrom(src=>"**********"+src.CardNumber)
                ).ForMember(des=>des.Date,
                    opt=>opt.MapFrom(src=>src.Date.ToShortDateString()));
            CreateMap<PaymentResponse, PaymentResponseDto>();
        }
    }
}
