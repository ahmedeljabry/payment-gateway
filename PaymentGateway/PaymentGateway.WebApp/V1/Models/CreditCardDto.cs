﻿
using PaymentGateway.Core.DomainObjects.Enums;
using System.ComponentModel.DataAnnotations;

namespace PaymentGateway.WebApp.V1.Models
{
    public class CreditCardDto
    {
        [Required]
        [StringLength(50, MinimumLength = 5)]
        public string CardholderName { get; set; }

        [CreditCard]
        [Required]
        public string CardNumber { get; set; }

        [Required]
        [Range(1, 12)]
        public Month Month { get; set; }

        [Required]
        [Range(20, 50)]
        public byte Year { get; set; }

        [Required]
        [StringLength(4, MinimumLength = 3)]
        public string Cvv { get; set; }

       
    }
}
