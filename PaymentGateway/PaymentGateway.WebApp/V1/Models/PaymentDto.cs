﻿namespace PaymentGateway.WebApp.V1.Models
{
    public class PaymentDto
    {
      
        public string CardNumber { get; set; }

        public string TransactionNumber { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }

        public string Status { get; set; }

        public string Date { get; set; }
    }
}
