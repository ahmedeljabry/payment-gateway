﻿

using System.ComponentModel.DataAnnotations;

namespace PaymentGateway.WebApp.V1.Models
{

    public class PaymentRequestDto
    {
        public CreditCardDto Card { get; set; }

        [Required]
        [Range(1, 100000)]
        public decimal Amount { get; set; }

        [Required]
        [StringLength(5, MinimumLength = 2)]
        public string Currency { get; set; }


    }

    
   
}
