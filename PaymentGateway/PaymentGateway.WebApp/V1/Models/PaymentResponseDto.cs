﻿using System.Net;

namespace PaymentGateway.WebApp.V1.Models
{
    public class PaymentResponseDto
    {
        public string TransactionNumber { get; set; }
        public HttpStatusCode StatusCode { get; set; }

        public string ErrorMessage { get; set; }
    }
}
