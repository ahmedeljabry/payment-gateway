# Payment Gatway #


### How do I get set up? ###

* Change connection string to your local DB in appsettings.json
* Rebuild solution
* To migrate EF core code first follow those instructions https://docs.microsoft.com/en-us/aspnet/core/data/ef-mvc/migrations
* Run all tests.
* WebApp is ready.

### TO_DO ###

* Docker 
* Test coverage ( now only 47%)
* Authentication and security https://docs.microsoft.com/en-us/dotnet/standard/security/how-to-store-asymmetric-keys-in-a-key-container

### Tools ###

  *  .NET Core 3.1
  *  .Net Core Mvc Versioning 4
  *  Entity Framework Core 3
  *  NUnit
  *  xUnit
  *  FluentAssertions
  *  Moq
  *  NLog
  *  AutoMapper
  *  Swagger
  *  Azure Function for simulating the acquiring bank Payment API